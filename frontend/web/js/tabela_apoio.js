/* View index Desejo */
$(function(){
    $('#btnModalCriarDesejo').click(function(){
        $('#modalCriarDesejo').modal('show')
            .find('#modalContentCriarDesejo')
            .load($(this).attr('value'));
    });
});

$('.btnModalAlterarDesejo').click(function () {
    $('#modalAlterarDesejo').modal('show')
        .find('#modalContentAlterarDesejo')
        .load($(this).attr('value'));
});

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poupanca */

$this->title = 'Create Poupanca';
$this->params['breadcrumbs'][] = ['label' => 'Poupancas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poupanca-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

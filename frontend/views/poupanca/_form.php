<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Poupanca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poupanca-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dsc_banco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taxa_juros')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

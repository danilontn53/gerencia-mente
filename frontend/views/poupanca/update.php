<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poupanca */

$this->title = 'Update Poupanca: ' . $model->id_poupanca;
$this->params['breadcrumbs'][] = ['label' => 'Poupancas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_poupanca, 'url' => ['view', 'id' => $model->id_poupanca]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="poupanca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

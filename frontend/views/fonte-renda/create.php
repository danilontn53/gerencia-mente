<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FonteRenda */

$this->title = 'Create Fonte Renda';
$this->params['breadcrumbs'][] = ['label' => 'Fonte Rendas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fonte-renda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

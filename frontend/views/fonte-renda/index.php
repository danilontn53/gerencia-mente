<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\FonteRendaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fonte Rendas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fonte-renda-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Fonte Renda', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_tipo_fonte_renda',
            'dsc_tipo_fonte_renda',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

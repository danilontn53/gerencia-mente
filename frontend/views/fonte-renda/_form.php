<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FonteRenda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fonte-renda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dsc_tipo_fonte_renda')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

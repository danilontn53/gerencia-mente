<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FonteRenda */

$this->title = 'Update Fonte Renda: ' . $model->id_tipo_fonte_renda;
$this->params['breadcrumbs'][] = ['label' => 'Fonte Rendas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tipo_fonte_renda, 'url' => ['view', 'id' => $model->id_tipo_fonte_renda]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fonte-renda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use kartik\dialog\Dialog;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Desejo */

$this->title = $model->dsc_desejo;
$this->params['breadcrumbs'][] = ['label' => 'Desejos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="desejo-view">

    <div class="pull-left">
        <a href="javascript:history.back()" class="btn btn-default btn_cin">Voltar</a>
    </div>
    <div class="pull-right">
        <?php
        echo Dialog::widget(['overrideYiiConfirm' => true]);

        echo Html::a('Apagar', ['delete', 'id' => $model->id_desejo], [
            'class' => 'btn btn-danger btn_verm btn_azul',
            'data' => [
                'confirm' => 'Tem certeza que deseja excluir este Desejo?',
                'method' => 'post',
            ],
        ]);
        ?>
    </div>

    <div style="margin-top: 2cm">
        <?= DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-striped table-bordered detail-view tab_perf_det'],
            'attributes' => [
                'dsc_desejo'
            ],
        ]) ?>
    </div>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Desejo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="desejo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dsc_desejo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Alterar', ['class' => $model->isNewRecord ? 'btn btn-primary btn_azul' : 'btn btn-primary btn_azul']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

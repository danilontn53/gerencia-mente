<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DesejoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Desejos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desejo-index">

    <p class="text_right">
        <?= Html::button('Criar Desejo', [
            'value' => Url::to(['create']),
            'class' => 'btn btn-primary btn_azul',
            'id' => 'btnModalCriarDesejo',
        ]) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Novo Desejo</h4>',
        'id'     => 'modalCriarDesejo',
        'size'   => 'model-lg',
    ]);

    echo "<div id='modalContentCriarDesejo'></div>";

    Modal::end();
    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Lista de Desejos</h3>
        </div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => '',
                'emptyText' => 'Nenhum Desejo encontrado.',
                'export' => false,
                'tableOptions' => ['class' => 'tab_perf'],
                'columns' => [
                    [
                        'attribute' => 'dsc_desejo',
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'headerOptions'=>['class'=>'col_acao'],
                        'contentOptions' => ['class' => 'col_acao_int'],
                        'header' => 'Ações',
                        'viewOptions' => ['title'=>'Visualizar'],
                        'deleteOptions' => ['label' => "<i class='glyphicon glyphicon-remove'></i>",
                            'title'=>'Excluir',
                            'data-confirm'=>'Tem certeza que deseja excluir este Desejo?'
                        ],
                        'buttons' => [
                            //update button
                            'update' => function ($url) {
                                return Html::button(null, [
                                    'value' => Url::to($url),
                                    'class' => 'btnModalAlterarDesejo glyphicon glyphicon-pencil',
                                    'style' => 'background: transparent; border: 0; outline: none; -webkit-appearance: none; color: #337AB7;',
                                    'title'=>'Editar',
                                ]);
                            },
                        ],
                    ],
                ],
                'krajeeDialogSettings' =>[
                    'options' => [
                        'title' =>'Aviso',
                        'btnOKLabel' =>'Sim',
                        'btnCancelLabel' =>'Não',
                    ],
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination paginacao_sgi',
                    ],
                ],
            ]); ?>
        </div>
    </div>


    <?php

    Modal::begin([
        'header'=> '<h4> Alterar Desejo</h4>',
        'id' => 'modalAlterarDesejo',
    ]);

    echo "<div id='modalContentAlterarDesejo'></div>";

    Modal::end();

    ?>

</div>

<?php $this->registerJs($this->render('@frontend/web/js/tabela_apoio.js')); ?>

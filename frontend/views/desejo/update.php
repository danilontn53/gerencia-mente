<?php

use yii\helpers\Html;


$this->title = 'Atualizar Desejo: ' . $model->dsc_desejo;
$this->params['breadcrumbs'][] = ['label' => 'Desejos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dsc_desejo, 'url' => ['view', 'id' => $model->dsc_desejo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="desejo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

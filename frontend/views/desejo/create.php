<?php

/* @var $this yii\web\View */
/* @var $model app\models\Desejo */

$this->title = 'Create Desejo';
$this->params['breadcrumbs'][] = ['label' => 'Desejos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desejo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

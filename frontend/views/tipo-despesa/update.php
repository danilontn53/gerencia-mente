<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoDespesa */

$this->title = 'Update Tipo Despesa: ' . $model->id_tipo_despesa;
$this->params['breadcrumbs'][] = ['label' => 'Tipo Despesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tipo_despesa, 'url' => ['view', 'id' => $model->id_tipo_despesa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipo-despesa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

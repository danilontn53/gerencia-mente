<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TipoDespesaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipo Despesas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-despesa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tipo Despesa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_tipo_despesa',
            'dsc_tipo_despesa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

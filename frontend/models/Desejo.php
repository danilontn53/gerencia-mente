<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "desejo".
 *
 * @property int $id_desejo
 * @property string $dsc_desejo
 *
 * @property UsuarioDesejo[] $usuarioDesejos
 */
class Desejo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'desejo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dsc_desejo'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_desejo' => 'Id Desejo',
            'dsc_desejo' => 'Descrição do Desejo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioDesejos()
    {
        return $this->hasMany(UsuarioDesejo::className(), ['desejo_id_desejo' => 'id_desejo']);
    }
}

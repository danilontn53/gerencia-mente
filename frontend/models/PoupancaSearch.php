<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Poupanca;

/**
 * PoupancaSearch represents the model behind the search form of `app\models\Poupanca`.
 */
class PoupancaSearch extends Poupanca
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_poupanca'], 'integer'],
            [['dsc_banco'], 'safe'],
            [['taxa_juros'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Poupanca::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_poupanca' => $this->id_poupanca,
            'taxa_juros' => $this->taxa_juros,
        ]);

        $query->andFilterWhere(['ilike', 'dsc_banco', $this->dsc_banco]);

        return $dataProvider;
    }
}

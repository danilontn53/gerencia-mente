<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fonte_renda".
 *
 * @property int $id_tipo_fonte_renda
 * @property string $dsc_tipo_fonte_renda
 *
 * @property UsuarioFonteRenda[] $usuarioFonteRendas
 */
class FonteRenda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fonte_renda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dsc_tipo_fonte_renda'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_fonte_renda' => 'Id Tipo Fonte Renda',
            'dsc_tipo_fonte_renda' => 'Dsc Tipo Fonte Renda',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioFonteRendas()
    {
        return $this->hasMany(UsuarioFonteRenda::className(), ['id_fonte_renda' => 'id_tipo_fonte_renda']);
    }
}

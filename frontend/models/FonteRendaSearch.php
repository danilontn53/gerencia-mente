<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FonteRenda;

/**
 * FonteRendaSearch represents the model behind the search form of `app\models\FonteRenda`.
 */
class FonteRendaSearch extends FonteRenda
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_fonte_renda'], 'integer'],
            [['dsc_tipo_fonte_renda'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FonteRenda::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tipo_fonte_renda' => $this->id_tipo_fonte_renda,
        ]);

        $query->andFilterWhere(['ilike', 'dsc_tipo_fonte_renda', $this->dsc_tipo_fonte_renda]);

        return $dataProvider;
    }
}

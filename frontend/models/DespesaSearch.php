<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Despesa;

/**
 * DespesaSearch represents the model behind the search form of `app\models\Despesa`.
 */
class DespesaSearch extends Despesa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_despesa', 'id_tipo_despesa'], 'integer'],
            [['dsc_despesa'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Despesa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_despesa' => $this->id_despesa,
            'id_tipo_despesa' => $this->id_tipo_despesa,
        ]);

        $query->andFilterWhere(['ilike', 'dsc_despesa', $this->dsc_despesa]);

        return $dataProvider;
    }
}

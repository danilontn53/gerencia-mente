<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "despesa".
 *
 * @property int $id_despesa
 * @property string $dsc_despesa
 * @property int $id_tipo_despesa
 *
 * @property TipoDespesa $tipoDespesa
 * @property UsuarioDespesa[] $usuarioDespesas
 */
class Despesa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'despesa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_despesa'], 'required'],
            [['id_tipo_despesa'], 'default', 'value' => null],
            [['id_tipo_despesa'], 'integer'],
            [['dsc_despesa'], 'string', 'max' => 250],
            [['id_tipo_despesa'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDespesa::className(), 'targetAttribute' => ['id_tipo_despesa' => 'id_tipo_despesa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_despesa' => 'Id Despesa',
            'dsc_despesa' => 'Dsc Despesa',
            'id_tipo_despesa' => 'Id Tipo Despesa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDespesa()
    {
        return $this->hasOne(TipoDespesa::className(), ['id_tipo_despesa' => 'id_tipo_despesa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioDespesas()
    {
        return $this->hasMany(UsuarioDespesa::className(), ['id_despesa' => 'id_despesa']);
    }
}

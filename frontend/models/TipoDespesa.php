<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_despesa".
 *
 * @property int $id_tipo_despesa
 * @property string $dsc_tipo_despesa
 *
 * @property Despesa[] $despesas
 */
class TipoDespesa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_despesa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dsc_tipo_despesa'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_despesa' => 'Id Tipo Despesa',
            'dsc_tipo_despesa' => 'Dsc Tipo Despesa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespesas()
    {
        return $this->hasMany(Despesa::className(), ['id_tipo_despesa' => 'id_tipo_despesa']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "poupanca".
 *
 * @property int $id_poupanca
 * @property string $dsc_banco
 * @property string $taxa_juros
 *
 * @property UsuarioPoupanca[] $usuarioPoupancas
 */
class Poupanca extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'poupanca';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taxa_juros'], 'number'],
            [['dsc_banco'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_poupanca' => 'Id Poupanca',
            'dsc_banco' => 'Dsc Banco',
            'taxa_juros' => 'Taxa Juros',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioPoupancas()
    {
        return $this->hasMany(UsuarioPoupanca::className(), ['poupanca_id_poupanca' => 'id_poupanca']);
    }
}

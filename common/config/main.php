<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'pt-br',
    'timezone' => 'America/Sao_Paulo',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => 'kartik\datecontrol\Module',
        ],
        'sliderrevolution' => [
            'class' => 'wadeshuler\sliderrevolution\SliderModule',
            'pluginLocation' => '@frontend/views/private/rs-plugin',    // <-- path to your rs-plugin directory
        ],
    ],

];
